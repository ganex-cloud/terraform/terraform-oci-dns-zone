variable "compartment_id" {
  description = "(Optional) (Updatable) The OCID of the compartment the resource belongs to. If supplied, it must match the Zone's compartment ocid."
  type        = string
  default     = ""
}

variable "defined_tags" {
  description = "(Optional) (Updatable) Defined tags for this resource. Each key is predefined and scoped to a namespace"
  type        = map(any)
  default     = {}
}

variable "freeform_tags" {
  description = "(Optional) (Updatable) Free-form tags for this resource. Each tag is a simple key-value pair with no predefined name, type, or namespace."
  type        = map(any)
  default     = {}
}

variable "name" {
  description = "(Required) The name of the zone."
  type        = string
  default     = ""
}

variable "zone_type" {
  description = "(Required) The type of the zone. Must be either PRIMARY or SECONDARY."
  type        = string
  default     = "PRIMARY"
}

variable "external_masters" {
  # https://www.terraform.io/docs/providers/oci/r/dns_zone.html#external_masters
  description = "(Optional) (Updatable) External master servers for the zone. externalMasters becomes a required parameter when the zoneType value is SECONDARY."
  type        = any
  default     = {}
}
