module "dns_zone_example-com" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-dns-zone.git?ref=master"
  compartment_id = module.compartiment_infra.compartment_id
  name           = "example.com"
}
