resource "oci_dns_zone" "this" {
  compartment_id = var.compartment_id
  name           = var.name
  zone_type      = var.zone_type
  defined_tags   = var.defined_tags
  freeform_tags  = var.freeform_tags

  dynamic "external_masters" {
    for_each = var.zone_type == "SECONDARY" ? list(1) : []
    content {
      address = lookup(external_masters.value, "address")
      port    = lookup(external_masters.value, "port")
      dynamic "tsig" {
        for_each = lookup(external_masters.value, "tsig")
        content {
          algorithm = lookup(tsig.value, "algorithm")
          name      = lookup(tsig.value, "name")
          secret    = lookup(tsig.value, "secret")
        }
      }
      tsig_key_id = lookup(external_masters.value, "tsig_key_id")
    }
  }
}
