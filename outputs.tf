output "dns_zone_id" {
  description = "The OCID of the zone."
  value       = oci_dns_zone.this.id
}

output "dns_zone_name" {
  description = "The name of the zone."
  value       = oci_dns_zone.this.name
}

output "dns_zone_zone_type" {
  description = "The type of the zone. Must be either PRIMARY or SECONDARY."
  value       = oci_dns_zone.this.zone_type
}

output "dns_zone_nameservers" {
  description = "The authoritative nameservers for the zone."
  value       = oci_dns_zone.this.*.nameservers
}
